
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

	 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container" id="movie_list">
	<h5> Movie List </h5>
	
</div>
<div id="movieDescriptionModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    &times;</button>
                <h4 class="modal-title">
                    Greetings
                </h4>
            </div>
            <div class="modal-body" id="description">
                
            </div>
            <div class="modal-footer">
                <input type="button" id="btnClosePopup" value="Close" class="btn btn-danger" data-dismiss="modal" />
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
 		 $.ajax({
                type:"GET",
                url:"https://api.themoviedb.org/4/list/7096014?page=1&api_key=4275cf25831de3b150d6ae572b31a179",
              
            success:function(response){
            	 for (var i = 0; i < response.results.length; i++) {
            	 	var movieId = response.results[i].id;
            	 	
                   document.getElementById('movie_list').innerHTML += '<ul class="list-group"><a id="movieGrid" onclick="return showMovieDetails(5)" href="#" movieId="movieId" style=" text-decoration: none;text-align:center;"><li class="list-group-item  list-group-item-primary">'+response.results[i].title+'</li></a></br></ul>';
                  
                }
                console.log(response.results[1].id);
                
            }
        });
});


	function showMovieDetails (movieId) {
		var movieId = 152601;
  		// var id = document.getElementById("movieGrid").getAttribute("movieId");


	   $.ajax({
                type:"GET",
                url:"https://api.themoviedb.org/3/movie/"+movieId+"?api_key=4275cf25831de3b150d6ae572b31a179",
              
            success:function(response){

            	document.getElementById('description').innerHTML += response.overview;
 	 			$("#movieDescriptionModal").modal("show");      
                console.log(response.overview);
                
            }
        });

	
    }

</script>
</body>
</html>

